import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisingPhotographyComponent } from './advertising-photography.component';

describe('AdvertisingPhotographyComponent', () => {
  let component: AdvertisingPhotographyComponent;
  let fixture: ComponentFixture<AdvertisingPhotographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisingPhotographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisingPhotographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
