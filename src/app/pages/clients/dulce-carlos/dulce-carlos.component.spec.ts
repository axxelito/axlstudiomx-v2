import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DulceCarlosComponent } from './dulce-carlos.component';

describe('DulceCarlosComponent', () => {
  let component: DulceCarlosComponent;
  let fixture: ComponentFixture<DulceCarlosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DulceCarlosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DulceCarlosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
