import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeilyComponent } from './keily.component';

describe('KeilyComponent', () => {
  let component: KeilyComponent;
  let fixture: ComponentFixture<KeilyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeilyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
