import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DulceEncuentroComponent } from './dulce-encuentro.component';

describe('DulceEncuentroComponent', () => {
  let component: DulceEncuentroComponent;
  let fixture: ComponentFixture<DulceEncuentroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DulceEncuentroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DulceEncuentroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
