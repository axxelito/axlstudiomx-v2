import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FatimaComponent } from './fatima.component';

describe('FatimaComponent', () => {
  let component: FatimaComponent;
  let fixture: ComponentFixture<FatimaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FatimaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FatimaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
