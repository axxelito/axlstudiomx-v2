import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrendaGriselComponent } from './brenda-grisel.component';

describe('BrendaGriselComponent', () => {
  let component: BrendaGriselComponent;
  let fixture: ComponentFixture<BrendaGriselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrendaGriselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrendaGriselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
