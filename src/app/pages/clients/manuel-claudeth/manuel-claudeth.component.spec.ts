import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuelClaudethComponent } from './manuel-claudeth.component';

describe('ManuelClaudethComponent', () => {
  let component: ManuelClaudethComponent;
  let fixture: ComponentFixture<ManuelClaudethComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuelClaudethComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuelClaudethComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
