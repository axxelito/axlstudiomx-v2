import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RamsesHidalgoComponent } from './ramses-hidalgo.component';

describe('RamsesHidalgoComponent', () => {
  let component: RamsesHidalgoComponent;
  let fixture: ComponentFixture<RamsesHidalgoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RamsesHidalgoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RamsesHidalgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
