import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DanielBelemComponent } from './daniel-belem.component';

describe('DanielBelemComponent', () => {
  let component: DanielBelemComponent;
  let fixture: ComponentFixture<DanielBelemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanielBelemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanielBelemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
