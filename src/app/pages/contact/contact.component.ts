import { Component, OnInit } from '@angular/core';
import {Message} from "../../classess/message";
import {FirebaseService} from "../../services/firebase.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  mMessage: Message;

  constructor(private afService: FirebaseService) {
    this.mMessage = new Message();
  }

  ngOnInit() {
  }

  fnSendMessage() {
    this.afService.db.collection('messages')
      .add(
        Object.assign({}, this.mMessage)
      ).then(() => {
      this.mMessage = new Message();
    });
  }

}
