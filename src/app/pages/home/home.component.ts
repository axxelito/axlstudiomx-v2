import { Component, OnInit } from '@angular/core';
import {FirebaseService} from "../../services/firebase.service";
import {Message} from "../../classess/message";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  mMessage: Message;

  constructor(private afService: FirebaseService) {
    this.mMessage = new Message();
  }

  ngOnInit() {
  }

  fnSendMessage() {
    this.afService.db.collection('messages')
      .add(
        Object.assign({}, this.mMessage)
      ).then(() => {
        this.mMessage = new Message();
    });
  }

}
