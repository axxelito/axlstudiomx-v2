import { Injectable } from '@angular/core';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/storage';
import 'firebase/firestore';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  firebase: any;
  db: any;
  mAuth: any;
  mStorageRef: any;

  constructor() { }

  fnInitializeFirebase() {
    firebase.initializeApp(environment.firebase);
    this.db = firebase.firestore();
    this.mAuth = firebase.auth();
    this.mStorageRef = firebase.storage().ref();
  }
}
