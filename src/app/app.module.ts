import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { DanielBelemComponent } from './pages/clients/daniel-belem/daniel-belem.component';
import { RamsesHidalgoComponent } from './pages/clients/ramses-hidalgo/ramses-hidalgo.component';
import { KeilyComponent } from './pages/clients/keily/keily.component';
import { FatimaComponent } from './pages/clients/fatima/fatima.component';
import { BrendaGriselComponent } from './pages/clients/brenda-grisel/brenda-grisel.component';
import { ManuelClaudethComponent } from './pages/clients/manuel-claudeth/manuel-claudeth.component';
import { DulceEncuentroComponent } from './pages/clients/dulce-encuentro/dulce-encuentro.component';
import { DulceCarlosComponent } from './pages/clients/dulce-carlos/dulce-carlos.component';
import { AdvertisingPhotographyComponent } from './pages/porfolios/advertising-photography/advertising-photography.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutUsComponent,
    PortfolioComponent,
    ClientsComponent,
    ContactComponent,
    NavbarComponent,
    FooterComponent,
    PrivacyPolicyComponent,
    DanielBelemComponent,
    RamsesHidalgoComponent,
    KeilyComponent,
    FatimaComponent,
    BrendaGriselComponent,
    ManuelClaudethComponent,
    DulceEncuentroComponent,
    DulceCarlosComponent,
    AdvertisingPhotographyComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
