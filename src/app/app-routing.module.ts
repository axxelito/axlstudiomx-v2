import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {PortfolioComponent} from './pages/portfolio/portfolio.component';
import {AboutUsComponent} from './pages/about-us/about-us.component';
import {ContactComponent} from './pages/contact/contact.component';
import {ClientsComponent} from './pages/clients/clients.component';
import {PrivacyPolicyComponent} from './pages/privacy-policy/privacy-policy.component';
import {DanielBelemComponent} from "./pages/clients/daniel-belem/daniel-belem.component";
import {KeilyComponent} from "./pages/clients/keily/keily.component";
import {BrendaGriselComponent} from "./pages/clients/brenda-grisel/brenda-grisel.component";
import {DulceEncuentroComponent} from "./pages/clients/dulce-encuentro/dulce-encuentro.component";
import {RamsesHidalgoComponent} from "./pages/clients/ramses-hidalgo/ramses-hidalgo.component";
import {FatimaComponent} from "./pages/clients/fatima/fatima.component";
import {ManuelClaudethComponent} from "./pages/clients/manuel-claudeth/manuel-claudeth.component";
import {DulceCarlosComponent} from "./pages/clients/dulce-carlos/dulce-carlos.component";
import {AdvertisingPhotographyComponent} from "./pages/porfolios/advertising-photography/advertising-photography.component";

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'portfolio', component: PortfolioComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'contact', component: ContactComponent},
  // CLIENTS
  {path: 'clients', component: ClientsComponent},
  // Weddings
  {path: 'wedding/daniel-belem', component: DanielBelemComponent},
  {path: 'wedding/brenda-grisel', component: BrendaGriselComponent},
  {path: 'wedding/manuel-claudeth', component: ManuelClaudethComponent},
  {path: 'wedding/dulce-carlos', component: DulceCarlosComponent},
  // XV
  {path: 'xv/fatima', component: FatimaComponent},
  {path: 'xv/keily', component: KeilyComponent},
  // Regular
  {path: 'client/ramses-hidalgo', component: RamsesHidalgoComponent},
  // Short Films
  {path: 'short-film/dulce-encuentro', component: DulceEncuentroComponent},
  // PORTFOLIOS
  {path: 'portfolio/advertising-photography', component: AdvertisingPhotographyComponent},
  // Legal
  {path: 'privacy-policy', component: PrivacyPolicyComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
