import { Component } from '@angular/core';
import {FirebaseService} from './services/firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'axlstudiomx-v2';

  constructor(private afservice: FirebaseService) {
    this.afservice.fnInitializeFirebase();
  }
}
