export class Message {
  name: string;
  email: string;
  phoneNumber: number;
  message: string;
}
